# My dotfiles

Credit/Inspiration:
- https://github.com/kespinola/dotfiles
- https://github.com/geerlingguy/mac-dev-playbook

## Pre-requisites

- Arch Linux:
    - `pacstrap /mnt base base-devel linux linux-firmware git iwd dhcpcd openssh lvm2`
- A user
- Root access

## Configurations (My Actual Dotfiles)
- You can either browse the various roles to see them **OR**
- You can visit the site: [https://dotfiles.jonnobrow.co.uk](https://dotfiles.jonnobrow.co.uk) *coming soon*

## Roles

- [x] *aur*: Install Packages and install [Yay](https://github.com/Jguer/yay)
- [x] *doom*: Install [emacs](https://www.gnu.org/software/emacs/), [doom emacs](https://github.com/hlissner/doom-emacs) and my emacs configurations
- [x] *gammastep*: Installs [geoclue](https://gitlab.freedesktop.org/geoclue/geoclue/-/wikis/home) and [gammastep](https://gitlab.com/chinstrap/gammastep), configures and enables services
- [x] *getty*: Overrides the default tty1 service to autologin the `login_username` user
- [x] *git*: Install my git config
- [x] *kitty*: Installs my [kitty](https://sw.kovidgoyal.net/kitty/) configuration
- [x] *mako*: Install my [mako](https://github.com/emersion/mako) configuration
- [x] *pacman*: Full system upgrade and update cache
- [ ] *ranger*: WIP
- [ ] *rtorrent*: WIP
- [x] *sway*: Installs my [sway](https://swaywm.org/) configuration
- [x] *syncthing*: Installs [syncthing](https://docs.syncthing.net/) and enables the service
- [ ] *tridactyl*: WIP
- [ ] *vim*: WIP
- [x] *vm*: Installs some scripts and the required packages for my Windows 10 virtual machine
    - Full install guide here: https://ssene.ca/posts/pci-passthrough/
    - *playbook may be a thing in the future?*
- [x] *wal*: Installs a couple of wal templates, [WAL](https://github.com/dylanaraps/pywal) and [WPGTK](https://github.com/deviantfero/wpgtk)
- [x] *waybar*: Installs [waybar](https://github.com/Alexays/Waybar) and my configuration
- [x] *wofi*: Installs [wofi](https://hg.sr.ht/~scoopta/wofi), my configuration and a script for running CSV files
- [ ] *zathura*: WIP
- [x] *zsh*: Installs [ZSH](https://www.zsh.org/) and some [ZSH Plugins](https://github.com/zsh-users) followed by my zsh configuration in XDG_CONFIG_DIR/zsh


## Bootstrap

A simple script that installs ansible and runs the playbook.

```shell
$ ./bin/dot-bootstrap
```

## Manual Config Checklist

- [ ] PCI Passthrough
- [ ] Setup File Sync with Syncthing
