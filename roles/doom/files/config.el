;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Hey look its me
(setq user-full-name "Jonathan Bartlett"
      user-mail-address "jonathan@jonnobrow.co.uk")

;; Make it look okay
(setq doom-theme 'doom-acario-dark
      doom-font (font-spec :family "Hack" :size 14)
      display-line-numbers t
      display-time-mode 1)
(toggle-frame-maximized)

;; ORG Basics
(setq org-directory "~/cloud/org/")
(add-hook! 'org-mode-hook
           (lambda () (org-bullets-mode 1)))
(after! org (setq org-hide-emphasis-markers t
                  org-hide-leading-stars t
                  org-ellipsis " ▼ "
                  org-bullets-bullet-list '("○" "☉" "◎" "◉" "○" "◌" "◎" "●" "◦")))

;; ORG Todo
(after! org
  (setq org-use-fast-todo-selection t)
  (setq org-todo-keywords (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                                  (sequence "WAITING(w/@!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)")
                                  (sequence "TOREAD(r)" "|" "READ(s@/!)"))))
  (setq org-todo-keyword-faces
        (quote (("TODO" :foreground "red" :weight bold)
                ("NEXT" :foreground "sky blue" :weight bold)
                ("DONE" :foreground "chartreuse" :weight bold)
                ("WAITING" :foreground "dark orange" :weight bold)
                ("HOLD" :foreground "light coral" :weight bold)
                ("TOREAD" :foreground "darkviolet" :weight bold)
                ("READ" :foreground "goldenrod" :weight bold)
                ("CANCELLED" :foreground "dark green" :weight bold))))

  (setq org-todo-state-tags-triggers
        (quote (("CANCELLED" ("CANCELLED" . t))
                ("WAITING" ("WAITING" . t))
                ("HOLD" ("WAITING") ("HOLD" . t)) ;; When we mark as HOLD, add HOLD and remove WAITING
                (done ("WAITING") ("HOLD"))       ;; Remove WAITING and HOLD
                ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                ("DONE" ("WAITING") ("CANCELLED") ("HOLD"))
                ("TOREAD" ("TOREAD" . t))
                ("READ" ("READ" . t))
                ))))

;; Capture
(after! org
  (setq org-capture-templates
        (quote (("t" "todo" entry (file "~/cloud/org/refile.org")
                 "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                ("n" "note" entry (file "~/cloud/org/refile.org")
                 "* %? :NOTE:+org-init-org-directory-h\n%U\n%a\n" :clock-in t :clock-resume t)
                ("j" "journal" entry (file+datetree "~/cloud/org/diary.org")
                 "* %?\n%U\n" :clock-in t :clock-resume t)
                ("l" "logbook" entry (file+datetree "~/cloud/org/work/LOGBOOK.org")
                 "* %?\n%U\n" :clock-in t :clock-resume t)))))

;; Refile
(after! org
  (setq org-refile-targets (quote ((nil :maxlevel . 9) ;; nil means current file
                                   (org-agenda-files :maxlevel . 9))))
  (setq org-refile-use-outline-path t)
  (setq org-outline-path-complete-in-steps nil)
  (setq org-refile-allow-creating-parent-nodes (quote confirm)))

;; Agenda
(after! org-agenda
  :config
  (setq org-agenda-skip-scheduled-if-done t
        org-agenda-skip-deadline-if-done t
        org-agenda-include-deadlines t
        org-agenda-dim-blocked-tasks nil
        org-agenda-inhibit-startup nil
        org-agenda-sticky t
        org-agenda-files (quote ("~/cloud/org/home"
                                 "~/cloud/org/work"
                                 "~/cloud/org/archive"
                                 "~/cloud/org"))
        org-agenda-show-future-repeats t
        org-agenda-block-separator 120
        org-deadline-warning-days 14
        org-agenda-compact-blocks nil
        org-agenda-start-day nil
        org-agenda-span 1
        org-agenda-start-on-weekday nil))

(defvar jb-agenda-block--refile
  '(tags "REFILE"
         ((org-agenda-overriding-header "Tasks to Refile")
          (org-tags-match-list-sublevels nil)))
  "A block showing the tasks I need to refile somewhere else")

(defvar jb-agenda-block--today
  '(agenda "" ((org-agenda-overriding-header "Today's Schedule:")
               (org-agenda-span 'day)
               (org-agenda-start-on-weekday nil)
               (org-agenda-start-day "+0d")))
  "A block showing my schedule for today")

(defvar jb-agenda-block--books-to-read
  '(tags-todo "-CANCELLED-WAITING-HOLD/!+TOREAD")
  "A block showing books to read")

(defvar jb-agenda-block--tasks
  '(tags-todo "-REFILE-CANCELLED-WAITING-HOLD-TOREAD-READ/!"
              ((org-agenda-overriding-header "Tasks")
               (org-agenda-sorting-strategy '(category-keep))
               (org-agenda-todo-ignore-scheduled t)
               (org-agenda-todo-ignore-deadlines t)
               (org-agenda-todo-ignore-with-date t)))
  "A block showing tasks")

(defvar jb-agenda-block--waiting-or-on-hold
  '(tags-todo "-CANCELLED+WAITING|HOLD/!"
              ((org-agenda-overriding-header "Waiting or On Hold")
               (org-tags-match-list-sublevels nil)
               (org-agenda-todo-ignore-scheduled t)
               (org-agenda-todo-ignore-deadlines t)
               (org-agenda-todo-ignore-with-date t)))
  "A block showing tasks waiting on something or put on hold")

(defvar jb-agenda-block--end-of-agenda
  '(tags "ENDOFAGENDA"
         ((org-agenda-overriding-header "End of Agenda")))
  "A block to mark the end of my agenda")

(after! org
  (setq org-agenda-custom-commands
        `(("N" "Notes" tags "NOTE"
                 ((org-agenda-overriding-header "Notes")
                  (org-tags-match-list-sublevels t)))
                (" " "Agenda"
                 (,jb-agenda-block--today
                  ,jb-agenda-block--books-to-read
                  ,jb-agenda-block--refile
                  ,jb-agenda-block--tasks
                  ,jb-agenda-block--end-of-agenda) nil)
                ("r " "Agenda Review (All)"
                 (,jb-agenda-block--refile
                  ,jb-agenda-block--waiting-or-on-hold
                  ,jb-agenda-block--end-of-agenda) nil)
                ("rw" "Agenda Review (Waiting or On Hold)"
                 (,jb-agenda-block--waiting-or-on-hold
                  ,jb-agenda-block--end-of-agenda) nil)
                )))


;; Tags
(after! org
  (setq org-tag-alist (quote ((:startgroup)
                              ("@home" . ?H)
                              ("@uni"  . ?u)
                              ("@errand" . ?e)
                              (:endgroup)
                              ("WAITING" . ?w)
                              ("HOLD" . ?h)
                              ("PERSONAL" . ?P)
                              ("UNI" . ?U)
                              ("PC" . ?p)
                              ("NOTE" . ?n)
                              ("CANCELLED" . ?c)
                              ("FLAGGED" . ??))))
  (setq org-agenda-tags-todo-honor-ignore-options t))

;; Archiving
(after! org
  (setq org-archive-mark-done nil
        org-archive-location "~/cloud/org/archive/%s_archive::"))


;; Babel
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (ipython . t)
   (gnuplot . t)))
(setq org-confirm-babel-evaluate nil)
(setq org-export-use-babel nil)
(add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)

;; Bindings
(map! :leader
      :prefix "n"
      "c" #'org-capture)
(map! "<f12>" #'org-agenda)
(map! "<f5>" #'org-todo)

;; Bibliography
(use-package org-ref
    :config
    (setq
         org-ref-completion-library 'org-ref-ivy-cite
         org-ref-get-pdf-filename-function 'org-ref-get-pdf-filename-helm-bibtex
         org-ref-default-bibliography (list "~/cloud/org/bibliography.bib")
         org-ref-bibliography-notes "~/cloud/org/bibnotes.org"
         org-ref-note-title-format (concat "* TODO %y - %t\n"
                                           ":PROPERTIES:\n"
                                           ":Custom_ID: %k\n"
                                           ":NOTER_DOCUMENT: %F\n"
                                           ":ROAM_KEY: cite:%k\n"
                                           ":AUTHOR: %9a\n"
                                           ":JOURNAL: %j\n"
                                           ":YEAR: %y\n"
                                           ":VOLUME: %v\n"
                                           ":PAGES: %p\n"
                                           ":DOI: %D\n"
                                           ":URL: %U\n"
                                           ":END:\n\n")
         org-ref-notes-directory "~/cloud/org/roam/lit/"
         org-ref-notes-function 'orb-edit-notes
         org-ref-pdf-directory "~/cloud/org/pdfs/"))

(after! org-ref
  (setq
 bibtex-completion-library-path "~/cloud/org/pdfs/"
 bibtex-completion-notes-path "~/cloud/org/roam/lit/"
 bibtex-completion-bibliography "~/cloud/org/bibliography.bib"
 bibtex-completion-pdf-field "file"
 bibtex-completion-notes-template-multiple-files
 (concat
  "#+TITLE: ${title}\n"
  "#+ROAM_KEY: cite:${=key=}\n"
  "* TODO Notes\n"
  ":PROPERTIES:\n"
  ":Custom_ID: ${=key=}\n"
  ":NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n"
  ":AUTHOR: ${author-abbrev}\n"
  ":JOURNAL: ${journaltitle}\n"
  ":DATE: ${date}\n"
  ":YEAR: ${year}\n"
  ":DOI: ${doi}\n"
  ":URL: ${url}\n"
  ":END:\n\n")))

(use-package org-noter
  :after (:any org pdf-view)
  :config
  (setq
   ;; The WM can handle splits
   org-noter-notes-window-location 'other-frame
   ;; Please stop opening frames
   org-noter-always-create-frame nil
   ;; I want to see the whole file
   org-noter-hide-other nil
   ;; Everything is relative to the main notes file
   org-noter-notes-search-path (list "~/cloud/org/roam/lit/")
   )
  )

(use-package! org-checklist)

;; Reminders
; Erase all reminders and rebuilt reminders for today from the agenda
(defun bh/org-agenda-to-appt ()
  (interactive)
  (setq appt-time-msg-list nil)
  (org-agenda-to-appt))

; Rebuild the reminders everytime the agenda is displayed
(add-hook 'org-agenda-finalize-hook 'bh/org-agenda-to-appt 'append)

; This is at the end of my .emacs - so appointments are set up when Emacs starts
(bh/org-agenda-to-appt)

; Activate appointments so we get notifications
(appt-activate t)

; If we leave Emacs running overnight - reset the appointments one minute after midnight
(run-at-time "24:01" nil 'bh/org-agenda-to-appt)

;; Diary
(setq diary-file "~/cloud/documents/diary")
(setq org-agenda-include-diary nil
      org-agenda-diary-file "~/cloud/org/diary.org")

;; Roam

(use-package! org-roam
  :defer t
  :config
  (setq org-roam-db-location "~/cloud/org/roam/org-roam.db"
        org-roam-directory "~/cloud/org/roam"
        org-roam-index-file (concat org-roam-directory "/" "roam-home.org")))

;; Roam Capture
(setq org-roam-capture-templates
        '(
          ("d" "default" plain (function org-roam-capture--get-point)
        "%?"
        :file-name "%(format-time-string \"notes/%Y-%m-%d--%H-%M-%SZ--${slug}\" (current-time) t)"
        :head "#+TITLE: ${title}
#+DATE: %T
#+HUGO_SLUG: ${slug}
#+ROAM_TAGS: "
        :unnarrowed t)))
;; Roam Server
(after! org-roam-server
  :config
  (setq org-roam-server-host "127.0.0.1"
        org-roam-server-port 8080
        org-roam-server-authenticate nil
        org-roam-server-export-inline-images t
        org-roam-server-serve-files nil
        org-roam-server-served-file-extensions '("pdf" "mp4" "ogv")
        org-roam-server-network-poll t
        org-roam-server-network-arrows nil
        org-roam-server-network-label-truncate t
        org-roam-server-network-label-truncate-length 60
        org-roam-server-network-label-wrap-length 20))
(use-package! org-roam-protocol
  :after org-protocol)

;; Roam Bibtex
(use-package! org-roam-bibtex
  :after (org-roam)
  :hook (org-roam-mode . org-roam-bibtex-mode)
  :config
  (setq org-roam-bibtex-preformat-keywords
        '("=key=" "title" "url" "file" "author-or-editor" "keywords"))
  (setq orb-templates
        `(("r" "ref" plain (function org-roam-capture--get-point)
           ""
           :file-name "lit/${slug}"
           :head ,(concat
                   "#+title: ${=key=}: ${title}\n"
                   "#+roam_key: ${ref}\n\n"
                   "* ${title}\n"
                   "  :PROPERTIES:\n"
                   "  :Custom_ID: ${=key=}\n"
                   "  :URL: ${url}\n"
                   "  :AUTHOR: ${author-or-editor}\n"
                   "  :NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n"
                   "  :NOTER_PAGE: \n"
                   "  :END:\n")
           :unnarrowed t))))

;; LaTeX
(setq reftex-default-bibliography '("~/cloud/org/bibliography.bib"))
(setq org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))

;; Deft
(use-package deft
  :after org
  :bind ("C-c n d" . deft)
  :custom
  (deft-recursive t)
  (deft-use-filter-string-for-filename t)
  (deft-default-extension "org")
  (deft-use-filename-as-title nil)
  (deft-file-naming-rules '((noslash . "-")
                            (nospace . "-")
                            (case-fn . downcase)))
  (deft-text-mode 'org-mode)
  (deft-directory "~/cloud/org/roam"))

;; Appt Notifications
(defun jb/appt-display-libnotify (min-to-app new-time msg)
  (jb/send-notification
   (format "Appointment in %s minutes" min-to-app)
   (format "%s" msg)))

(defun jb/send-notification (title msg)
  (let ((notifier-path (executable-find "notify-send")))
    (start-process
     "Appointment Alert"
     "*Appointment Alert*"
     notifier-path
     title
     msg
     "-a" "Emacs"
     "-i" "emacs")))

(after! appt
  (setq appt-time-msg-list nil
        appt-display-interval '2
        appt-message-warning-time '10
        appt-display-mode-line nil
        appt-display-format 'window
        appt-disp-window-function (function jb/appt-display-libnotify)))
