# XDG Config Directory
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

# Enable WAYLAND
export KITTY_ENABLE_WAYLAND=1
export MOZ_ENABLE_WAYLAND=1
export XDG_SESSION_TYPE=wayland
export _JAVA_AWT_WM_NONREPARENTING=1
export SDL_VIDEODRIVER=wayland

# Set QT options
export QT_QPA_PLATFORM=wayland
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export QT_QPA_PLATFORMTHEME=gtk2

# Set Browser
export BROWSER="/usr/bin/firefox-developer-edition"

# Set Editors
export EDITOR="vim"

# Set Terminal Command
export TERMCMD=kitty

# Set default opener
export OPENER="mimeopen"

# Add Local Bin to PATH
export PATH="$PATH:$HOME/.local/bin"

# Rust Cargo
export CARGO_HOME="$XDG_DATA_HOME/cargo"

# NPM
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc

# GTK 2 RC
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
