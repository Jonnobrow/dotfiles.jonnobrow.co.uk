# enable colours and change prompt:
autoload -U colors && colors
PS1="%F{cyan}[%f%F{red}%m%f%F{cyan}:%f%B%F{magenta}%~%f%b%F{cyan}]%f%F{white}$%f "

# history
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history
setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST # expire duplicates first
setopt HIST_IGNORE_DUPS # do not store duplications
setopt HIST_FIND_NO_DUPS # ignore duplicates when searching
setopt HIST_REDUCE_BLANKS # removes blank lines from history

# basic auto/tab complete
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)	# include hidden files

# vi mode
bindkey -v
export KEYTIMEOUT=1

# use vi keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# edit line in default editor with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

## safer cp/rm/mv operations
alias cp="cp -iv"
alias mv="mv -iv"

# Substring Search
zle -N history-substring-search-up
zle -N history-substring-search-down

## vi bindings
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

## arrow keys
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

source ~/.local/share/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.local/share/zsh/zsh-history-substring-search/zsh-history-substring-search.zsh
source ~/.local/share/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
fpath=(~/.local/share/zsh/zsh-completions/ $fpath)

## WAL
# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
(cat ~/.cache/wal/sequences &)

# To add support for TTYs this line can be optionally added.
source ~/.cache/wal/colors-tty.sh
